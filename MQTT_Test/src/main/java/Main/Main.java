package Main;

import com.DHBW.Mapper.OpenHab_Ontology_Mapper;

public class Main {
	public static void main(String[] args) throws Exception {
		String ontology_file_path = "./Ontologies/Smart_Kitchen_Refined.owl";
		@SuppressWarnings("unused")
		OpenHab_Ontology_Mapper openhab_protege_mapper = new OpenHab_Ontology_Mapper(ontology_file_path);
	}
}
