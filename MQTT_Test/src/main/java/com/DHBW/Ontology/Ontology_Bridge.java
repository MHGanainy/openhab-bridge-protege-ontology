package com.DHBW.Ontology;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.InferenceType;
import com.clarkparsia.pellet.owlapiv3.PelletReasoner;

public class Ontology_Bridge {
	@SuppressWarnings("unused")
	private String ontology_file_path;
	private OWLOntologyManager ontology_manager;
	@SuppressWarnings("unused")
	private OWLDataFactory ontology_data_factory;
	private OWLOntology ontology;
	private PelletReasoner ontology_reasoner;
	@SuppressWarnings("unused")
	private ArrayList<Ontology_Object> Ontology_Objects = new ArrayList<Ontology_Object>();

	public Ontology_Bridge(String ontology_file_path) {
		try {
			this.ontology_file_path = ontology_file_path;
			this.ontology_manager = OWLManager.createOWLOntologyManager();
			this.ontology_data_factory = OWLManager.getOWLDataFactory();
			this.ontology = ontology_manager.loadOntologyFromOntologyDocument(IRI.create(new File(ontology_file_path)));
			this.ontology_reasoner = com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory.getInstance()
					.createReasoner(this.ontology);

		} catch (OWLOntologyCreationException e) {
			System.out.println("Failed To Load Ontology");
			e.printStackTrace();
		}
		this.ontology_reasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY);
	}

	public void Display_Ontology_Objects() {
		ArrayList<Ontology_Object> Ontology_Objects = this.Ontology_Objects;
		System.out.println();
		System.out.println("***********************************************************************************");
		for (Ontology_Object Ontology_Object : Ontology_Objects) {
			System.out.println(Ontology_Object);
			System.out.println("***********************************************************************************");
		}
		System.out.println();
	}

	public ArrayList<Ontology_Object> Parse_Ontology_Object() {
		Set<OWLAxiom> All_ontology_Axioms = this.ontology.getAxioms();
		for (Iterator<OWLAxiom> iterator = All_ontology_Axioms.iterator(); iterator.hasNext();) {
			OWLAxiom owlAxiom = (OWLAxiom) iterator.next();
			String string_of_owlAxiom = owlAxiom.toString();
			if (StringUtils.startsWith(string_of_owlAxiom, "DataPropertyAssertion")) {
				OWLDataPropertyAssertionAxiom ontology_data_property_axiom = (OWLDataPropertyAssertionAxiom) owlAxiom;

				TreeSet<OWLDataProperty> ontology_data_property_set = new TreeSet<OWLDataProperty>(
						ontology_data_property_axiom.getDataPropertiesInSignature());
				OWLDataProperty ontology_data_property = ontology_data_property_set.first();

				TreeSet<OWLDataPropertyRangeAxiom> ontology_data_property_range_axiom_set = new TreeSet<OWLDataPropertyRangeAxiom>(
						this.ontology.getDataPropertyRangeAxioms(ontology_data_property));
				OWLDataPropertyRangeAxiom ontology_data_property_range = ontology_data_property_range_axiom_set.first();

				TreeSet<OWLDatatype> ontology_data_type_set = new TreeSet<OWLDatatype>(
						ontology_data_property_range.getDatatypesInSignature());
				OWLDatatype ontology_data_type = ontology_data_type_set.first();

				TreeSet<OWLNamedIndividual> ontology_individual_set = new TreeSet<OWLNamedIndividual>(
						ontology_data_property_axiom.getIndividualsInSignature());
				OWLNamedIndividual ontology_individual = ontology_individual_set.first();

				TreeSet<OWLClassAssertionAxiom> ontology_class_assertion_set = new TreeSet<OWLClassAssertionAxiom>(
						this.ontology.getClassAssertionAxioms(ontology_individual));
				OWLClassAssertionAxiom ontology_class_assertion = ontology_class_assertion_set.first();

				TreeSet<OWLClass> ontology_class_set = new TreeSet<OWLClass>(
						ontology_class_assertion.getClassesInSignature());
				OWLClass ontology_class = ontology_class_set.first();

				String onotlogy_data_property_literal = ontology_data_property_axiom.getObject().getLiteral();
				Ontology_Object Ontology_Object = new Ontology_Object(ontology_class, ontology_individual,
						ontology_data_property_axiom, ontology_data_property, ontology_data_type,
						onotlogy_data_property_literal);
				if (!Ontology_Objects.contains(Ontology_Object)) {
					this.Ontology_Objects.add(Ontology_Object);
				}

			}
		}
		parse_Ontology_Object_Inferences();
		addOpenHABIdentifier();
		filterOntologyArray();
		return Ontology_Objects;
	}

	public String getValue(Object o) {
		String s = o.toString();
		String acc = "";
		boolean begin = false;
		for (int i = 0; i < s.length(); i++) {
			if (begin) {
				if (s.charAt(i) != '>') {
					acc += s.charAt(i);
				}
			} else if (s.charAt(i) == '#') {
				begin = true;
			}
		}
		return acc;
	}

	public void parse_Ontology_Object_Inferences() {
		Set<OWLNamedIndividual> All_ontology_individuals = this.ontology.getIndividualsInSignature();
		for (OWLNamedIndividual ontology_individual : All_ontology_individuals) {

			TreeSet<OWLClassAssertionAxiom> ontology_class_assertion_set = new TreeSet<OWLClassAssertionAxiom>(
					this.ontology.getClassAssertionAxioms(ontology_individual));
			OWLClassAssertionAxiom ontology_class_assertion = ontology_class_assertion_set.first();

			TreeSet<OWLClass> ontology_class_set = new TreeSet<OWLClass>(
					ontology_class_assertion.getClassesInSignature());
			OWLClass ontology_class = ontology_class_set.first();

			for (OWLDataProperty ontology_data_property : this.ontology.getDataPropertiesInSignature()) {
				TreeSet<OWLLiteral> ontology_individual_data_property_literal = new TreeSet<OWLLiteral>(
						this.ontology_reasoner.getDataPropertyValues(ontology_individual, ontology_data_property));

				if (!ontology_individual_data_property_literal.isEmpty()) {
					String onotlogy_data_property_literal = ontology_individual_data_property_literal.first()
							.getLiteral();

					TreeSet<OWLDataPropertyRangeAxiom> ontology_data_property_range_axiom_set = new TreeSet<OWLDataPropertyRangeAxiom>(
							this.ontology.getDataPropertyRangeAxioms(ontology_data_property));
					OWLDataPropertyRangeAxiom ontology_data_property_range = ontology_data_property_range_axiom_set
							.first();

					TreeSet<OWLDatatype> ontology_data_type_set = new TreeSet<OWLDatatype>(
							ontology_data_property_range.getDatatypesInSignature());
					OWLDatatype ontology_data_type = ontology_data_type_set.first();
					Ontology_Object Ontology_Object = new Ontology_Object(ontology_class, ontology_individual,
							ontology_data_property, ontology_data_type, onotlogy_data_property_literal);
					if (!this.Ontology_Objects.contains(Ontology_Object)) {
						this.Ontology_Objects.add(Ontology_Object);
					} else if (this.Ontology_Objects.contains(Ontology_Object)) {
						int position = this.Ontology_Objects.indexOf(Ontology_Object);

						String Literal1 = this.Ontology_Objects.get(position).getOntology_data_literal();
						String Literal2 = Ontology_Object.getOntology_data_literal();

						boolean LiteralEquality = Literal1.equals(Literal2);

						if (!LiteralEquality) {
							this.Ontology_Objects.get(position)
									.setOntology_data_literal(Ontology_Object.getOntology_data_literal());
						}
					}

				}
			}
		}

	}

	public void Edit_Ontology_Object(Ontology_Object Ontology_Object, String ontology_data_literal_new) {
		OWLAxiom ontology_data_property_axiom_old = Ontology_Object.getOntology_data_property_assertion_axiom();
		OWLAxiom ontology_data_property_axiom_new = null;

		if (Ontology_Object.getOntology_data_type_String().equals("string")) {
			ontology_data_property_axiom_new = this.ontology_data_factory.getOWLDataPropertyAssertionAxiom(
					Ontology_Object.getOntology_data_property(), Ontology_Object.getOntology_individual(),
					ontology_data_literal_new);
		} else if (Ontology_Object.getOntology_data_type_String().equals("integer")) {
			ontology_data_property_axiom_new = this.ontology_data_factory.getOWLDataPropertyAssertionAxiom(
					Ontology_Object.getOntology_data_property(), Ontology_Object.getOntology_individual(),
					Integer.parseInt(ontology_data_literal_new));
		} else if (Ontology_Object.getOntology_data_type_String().equals("boolean")) {
			ontology_data_property_axiom_new = this.ontology_data_factory.getOWLDataPropertyAssertionAxiom(
					Ontology_Object.getOntology_data_property(), Ontology_Object.getOntology_individual(),
					Boolean.parseBoolean(ontology_data_literal_new));
		} else if (Ontology_Object.getOntology_data_type_String().equals("float")) {
			ontology_data_property_axiom_new = this.ontology_data_factory.getOWLDataPropertyAssertionAxiom(
					Ontology_Object.getOntology_data_property(), Ontology_Object.getOntology_individual(),
					Float.parseFloat(ontology_data_literal_new));
		} else {
			System.out.println("Undefined DataType");
			return;
		}

		Ontology_Object.setOntology_data_literal(ontology_data_literal_new);
		Ontology_Object.setOntology_data_property_assertion_axiom(
				(OWLDataPropertyAssertionAxiom) ontology_data_property_axiom_new);

		AddAxiom NewAxiom = new AddAxiom(this.ontology, ontology_data_property_axiom_new);
		this.ontology_manager.removeAxiom(this.ontology, ontology_data_property_axiom_old);
		this.ontology_reasoner.refresh();
		this.ontology_manager.applyChange(NewAxiom);
		this.ontology_reasoner.refresh();
		Parse_Ontology_Object();
	}

	public void addOpenHABIdentifier() {
		ArrayList<Ontology_Object> OpenHab_Identifier_Array = new ArrayList<Ontology_Object>();
		for (Ontology_Object po : this.Ontology_Objects) {
			String DataProperty = po.getOntology_data_property_String();
			if (DataProperty.equals("OpenHab_Identifier")) {
				OpenHab_Identifier_Array.add(po);
			}
		}
		for (Ontology_Object Ontology_Object : OpenHab_Identifier_Array) {
			for (Ontology_Object Ontology_Object2 : this.Ontology_Objects) {
				String IdentifierClassName = Ontology_Object.getOntology_class_String();
				String ClassName = Ontology_Object2.getOntology_class_String();
				String IdentifierIndividual = Ontology_Object.getOntology_individual_String();
				String Individual = Ontology_Object2.getOntology_individual_String();
				String IdentifierDataProperty = Ontology_Object.getOntology_data_property_String();
				String DataProperty = Ontology_Object2.getOntology_data_property_String();
				if (IdentifierClassName.equals(ClassName) && IdentifierIndividual.equals(Individual)
						&& !IdentifierDataProperty.equals(DataProperty)) {
					Ontology_Object2.setOpenhab_identifier(Ontology_Object.getOntology_data_literal());
				}
			}
		}
	}

	public void filterOntologyArray() {
		ArrayList<Ontology_Object> Ontology_Objects_Filtered = new ArrayList<Ontology_Object>();
		for (Ontology_Object ontology_Object : Ontology_Objects) {
			if(!ontology_Object.getOpenhab_identifier().equals(""))
			Ontology_Objects_Filtered.add(ontology_Object);
		}
		Ontology_Objects.clear();
		Ontology_Objects.addAll(Ontology_Objects_Filtered);
	}
	
	public static void main(String[] args) {
		Ontology_Bridge pb = new Ontology_Bridge("./Ontologies/Smart_Kitchen_Refined.owl");
		pb.Parse_Ontology_Object();
		pb.Display_Ontology_Objects();
	}
}
