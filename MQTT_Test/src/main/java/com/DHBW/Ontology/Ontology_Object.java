package com.DHBW.Ontology;

import java.util.Observable;

import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLNamedIndividual;

public class Ontology_Object extends Observable {
	private OWLClass ontology_class;
	private OWLNamedIndividual ontology_individual;
	private OWLDataPropertyAssertionAxiom ontology_data_property_assertion_axiom;
	private OWLDataProperty ontology_data_property;
	private OWLDatatype ontology_data_type;
	private String ontology_data_literal;
	private String openhab_ontology_link;
	private String openhab_identifier;

	public Ontology_Object(OWLClass ontology_class, OWLNamedIndividual ontology_individual,
			OWLDataPropertyAssertionAxiom ontology_data_property_assertion_axiom,
			OWLDataProperty ontology_data_property, OWLDatatype ontology_data_type, String ontology_data_literal) {
		this.ontology_class = ontology_class;
		this.ontology_individual = ontology_individual;
		this.ontology_data_property_assertion_axiom = ontology_data_property_assertion_axiom;
		this.ontology_data_property = ontology_data_property;
		this.ontology_data_type = ontology_data_type;
		this.ontology_data_literal = ontology_data_literal;
		this.openhab_ontology_link = getOntology_class_String()+"/"+getOntology_individual_String()+"/"+getOntology_data_property_String();
		this.openhab_identifier="";
	}

	public Ontology_Object(OWLClass ontology_class, OWLNamedIndividual ontology_individual,
			OWLDataProperty ontology_data_property, OWLDatatype ontology_data_type, String ontology_data_literal) {
		this.ontology_class = ontology_class;
		this.ontology_individual = ontology_individual;
		this.ontology_data_property_assertion_axiom = null;
		this.ontology_data_property = ontology_data_property;
		this.ontology_data_type = ontology_data_type;
		this.ontology_data_literal = ontology_data_literal;
		this.openhab_ontology_link = getOntology_class_String()+"/"+getOntology_individual_String()+"/"+getOntology_data_property_String();
		this.openhab_identifier="";
	}

	// -------------Object Format----------------
	public OWLClass getOntology_class() {
		return ontology_class;
	}

	public void setOntology_class(OWLClass ontology_class) {
		this.ontology_class = ontology_class;
	}

	public OWLNamedIndividual getOntology_individual() {
		return ontology_individual;
	}

	public void setOntology_individual(OWLNamedIndividual ontology_individual) {
		this.ontology_individual = ontology_individual;
	}

	public OWLDataPropertyAssertionAxiom getOntology_data_property_assertion_axiom() {
		return ontology_data_property_assertion_axiom;
	}

	public void setOntology_data_property_assertion_axiom(
			OWLDataPropertyAssertionAxiom ontology_data_property_assertion_axiom) {
		this.ontology_data_property_assertion_axiom = ontology_data_property_assertion_axiom;
	}

	public OWLDataProperty getOntology_data_property() {
		return ontology_data_property;
	}

	public void setOntology_data_property(OWLDataProperty ontology_data_property) {
		this.ontology_data_property = ontology_data_property;
	}

	public OWLDatatype getOntology_data_type() {
		return ontology_data_type;
	}

	public void setOntology_data_type(OWLDatatype ontology_data_type) {
		this.ontology_data_type = ontology_data_type;
	}

	public String getOntology_data_literal() {
		return ontology_data_literal;
	}

	public void setOntology_data_literal(String ontology_data_literal) {
		synchronized (this) {
			this.ontology_data_literal = ontology_data_literal;
		}
		setChanged();
		notifyObservers(this);
	}
	

	public String getOpenhab_ontology_link() {
		return openhab_ontology_link;
	}

	public void setOpenhab_ontology_link(String openhab_ontology_link) {
		this.openhab_ontology_link = openhab_ontology_link;
	}

	public String getOpenhab_identifier() {
		return openhab_identifier;
	}

	public void setOpenhab_identifier(String openhab_identifier) {
		this.openhab_identifier = openhab_identifier;
	}
	// -------------String Format----------------
	public String getOntology_class_String() {
		return getValue(ontology_class.getIRI().toString());
	}

	public String getOntology_individual_String() {
		return getValue(ontology_individual.getIRI().toString());
	}

	public String getOntology_data_property_String() {
		return getValue(ontology_data_property.getIRI().toString());
	}

	public String getOntology_data_type_String() {
		return getValue(ontology_data_type.getIRI().toString());
	}

	// --------------Helper Method------------------
	public String getValue(Object o) {
		String s = o.toString();
		String acc = "";
		boolean begin = false;
		for (int i = 0; i < s.length(); i++) {
			if (begin) {
				if (s.charAt(i) != '>') {
					acc += s.charAt(i);
				}
			} else if (s.charAt(i) == '#') {
				begin = true;
			}
		}
		return acc;
	}


	public String toString_Normal() {
		return "Protege_Object [ontology_class=" + ontology_class + ", ontology_individual=" + ontology_individual
				+ ", ontology_data_property_assertion_axiom=" + ontology_data_property_assertion_axiom
				+ ", ontology_data_property=" + ontology_data_property + ", ontology_data_type=" + ontology_data_type
				+ ", ontology_data_literal=" + ontology_data_literal + "]";
	}

	@Override
	public String toString() {
		String ontology_class = getOntology_class_String();
		String ontology_individual = getOntology_individual_String();
		String ontology_data_property = getOntology_data_property_String();
		String ontology_data_type = getOntology_data_type_String();
		String ontology_data_literal = getOntology_data_literal();

		String result1 = "* Class= " + ontology_class + " Individual= " + ontology_individual;
		String result2 = "\n* DataProperty= " + ontology_data_property + " DataType= " + ontology_data_type
				+ " DataLiteral= " + ontology_data_literal+" OpenHab_Identifier= "+getOpenhab_identifier();

		int result1_size = result1.length();
		int result2_size = result2.length();

		if (result1_size < 83) {
			int missing_spaces = 83 - result1_size - 1;
			for (int i = 0; i < missing_spaces; i++) {
				result1 += " ";
			}
			result1 += "*";
		}

		if (result2_size < 83) {
			int missing_spaces = 83 - result2_size;
			for (int i = 0; i < missing_spaces; i++) {
				result2 += " ";
			}
			result2 += "*";
		}

		return result1 + result2;
	}

	@Override
	public boolean equals(Object obj) {
		Ontology_Object Protege_Obj = (Ontology_Object) obj;
		String ontology_class = Protege_Obj.getOntology_class_String();
		String ontology_individual = Protege_Obj.getOntology_individual_String();
		String ontology_data_property = Protege_Obj.getOntology_data_property_String();
		String ontology_data_type = Protege_Obj.getOntology_data_type_String();

		boolean ontology_class_boolean = ontology_class.equals(getOntology_class_String());
		boolean ontology_individual_boolean = ontology_individual.equals(getOntology_individual_String());
		boolean ontology_data_property_boolean = ontology_data_property.equals(getOntology_data_property_String());
		boolean ontology_data_type_boolean = ontology_data_type.equals(getOntology_data_type_String());

		return ontology_class_boolean && ontology_individual_boolean && ontology_data_property_boolean
				&& ontology_data_type_boolean;
	}

}