package com.DHBW.OpenHab;

import java.util.ArrayList;

public class OpenHab_Item {
	private String link;
	private String state;
	private String type;
	private String name;
	private ArrayList<String> tags;
	private ArrayList<String> groupNames;
	
	public OpenHab_Item(String link, String state, String type, String name, ArrayList<String> tags,ArrayList<String> groupNames) {
		this.link = link;
		this.state = state;
		this.type = type;
		this.name = name;
		this.tags = tags;
		this.groupNames = groupNames;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public ArrayList<String> getGroupNames() {
		return groupNames;
	}

	public void setGroupNames(ArrayList<String> groupNames) {
		this.groupNames = groupNames;
	}
	
	public String getTagsconcatenated() {
		String tagconcatenated ="";
		for (String tag : tags) {
			tagconcatenated+= tag;
		}
		return tagconcatenated;
	}

	@Override
	public boolean equals(Object obj) {
		OpenHab_Item openhab_item = (OpenHab_Item) obj;
		
		String tagsconcat = "";
		
		ArrayList<String> obj_tags = openhab_item.getTags();
		
		for (String tag : obj_tags) {
			tagsconcat += tag;
		}
		
		return tagsconcat.equals(getTagsconcatenated());
	}

	@Override
	public String toString() {
		return "OpenHab_Item [link=" + link + ", state=" + state + ", type=" + type + ", name=" + name + ", tag=" + tags + ", groupNames=" + groupNames + "]";
	}
	
	
}
