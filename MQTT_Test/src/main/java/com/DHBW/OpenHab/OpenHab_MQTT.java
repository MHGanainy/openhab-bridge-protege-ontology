package com.DHBW.OpenHab;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class OpenHab_MQTT implements MqttCallback {
	private MqttClient OpenHab_MQTT_Client;

	public OpenHab_MQTT() {
		createClient("tcp://127.0.0.1:1883", "openHab_Bridge");
		subscribe();
	}

	@Override
	public void connectionLost(Throwable cause) {
		// TODO Auto-generated method stub
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
	}

	public void createClient(String broker, String clientID) {
		try {
			MemoryPersistence persistence = new MemoryPersistence();
			OpenHab_MQTT_Client = new MqttClient(broker, clientID, persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setMaxInflight(30);
			connOpts.setCleanSession(true);
			OpenHab_MQTT_Client.connect(connOpts);
			System.out.println("Connected");
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void subscribe() {
		try {
			OpenHab_MQTT_Client.setCallback(this);
			OpenHab_MQTT_Client.subscribe("#");
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

	public void unsubscribe() throws MqttException {
		OpenHab_MQTT_Client.unsubscribe("#");
	}

}
