package com.DHBW.OpenHab;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.BasicConfigurator;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class OpenHab_Bridge {
	private ArrayList<OpenHab_Item> OpenHab_Items = new ArrayList<OpenHab_Item>();

	public ArrayList<OpenHab_Item> getItems() throws Exception {
		OpenHab_Items.clear();
		BasicConfigurator.configure();
		RestTemplate restTemplate = new RestTemplate();
		String URL = "http://127.0.0.1:8080/rest/items?recursive=false";
		ResponseEntity<String> response = restTemplate.getForEntity(URL, String.class);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = mapper.readTree(response.getBody());
		for (Iterator<JsonNode> iterator = root.iterator(); iterator.hasNext();) {
			JsonNode node = (JsonNode) iterator.next();

			String link = node.get("link").asText();
			String state = node.get("state").asText();
			String type = node.get("type").asText();
			String name = node.get("name").asText();
			ArrayList<String> tags = new ArrayList<String>();
			ArrayList<String> groupNames = new ArrayList<String>();
			JsonNode tags_Node = node.get("tags");
			JsonNode groupNames_Node = node.get("groupNames");

			for (Iterator<JsonNode> iterator2 = tags_Node.iterator(); iterator2.hasNext();) {
				JsonNode tag = (JsonNode) iterator2.next();
				tags.add(tag.asText());
			}

			for (Iterator<JsonNode> iterator3 = groupNames_Node.iterator(); iterator3.hasNext();) {
				JsonNode groupName = (JsonNode) iterator3.next();
				groupNames.add(groupName.asText());
			}
			if(!type.equals("Group")) {
			OpenHab_Item openhab_item = new OpenHab_Item(link, state, type, name, tags, groupNames);
			if(!OpenHab_Items.contains(openhab_item)) {
				OpenHab_Items.add(openhab_item);
			}else if(OpenHab_Items.contains(openhab_item)){
				throw new Exception("Unique Tags are not met");
			}
			}
		}
		return OpenHab_Items;
	}

	public OpenHab_Item getItem(String Item_name) {
		for (OpenHab_Item openHab_Item : this.OpenHab_Items) {
			String openHab_Item_Name = openHab_Item.getName();
			if (openHab_Item_Name.equals(Item_name)) {
				return openHab_Item;
			}
		}

		System.out.println("Item Not Found");
		return null;
	}

	public String getState(String Item_name) {
		OpenHab_Item openhab_item = getItem(Item_name);
		if (openhab_item != null) {
			BasicConfigurator.configure();
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.getForEntity(openhab_item.getLink() + "/state",
					String.class);
			return response.getBody();
		}
		System.out.println("Error in Getting State of the Specified Item");
		return null;
	}

	public void setState(String Item_name, String value) throws Exception {
		OpenHab_Item openhab_item = getItem(Item_name);
		URL url = new URL(openhab_item.getLink() + "/state");
		HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setRequestMethod("PUT");
		OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
		out.write(value);
		out.close();
		httpCon.getInputStream();
		getItems();
	}
	
	public static void main(String[] args) throws Exception {
		OpenHab_Bridge op = new OpenHab_Bridge();
		System.out.println(op.getItems());
		
	}
}
