package com.DHBW.Mapper;

import com.DHBW.Ontology.Ontology_Object;
import com.DHBW.OpenHab.OpenHab_Item;

public class OpenHab_Ontology_Link {
	private OpenHab_Item openhab_item;
	private Ontology_Object Ontology_object;
	private String openhab_Ontology_link;
	
	public OpenHab_Ontology_Link(OpenHab_Item openhab_item, Ontology_Object Ontology_object,
			String openhab_Ontology_link) {
		this.openhab_item = openhab_item;
		this.Ontology_object = Ontology_object;
		this.openhab_Ontology_link = openhab_Ontology_link;
	}

	public OpenHab_Item getOpenhab_item() {
		return openhab_item;
	}

	public void setOpenhab_item(OpenHab_Item openhab_item) {
		this.openhab_item = openhab_item;
	}

	public Ontology_Object getOntology_object() {
		return Ontology_object;
	}

	public void setOntology_object(Ontology_Object Ontology_object) {
		this.Ontology_object = Ontology_object;
	}

	public String getOpenhab_Ontology_link() {
		return openhab_Ontology_link;
	}

	public void setOpenhab_Ontology_link(String openhab_Ontology_link) {
		this.openhab_Ontology_link = openhab_Ontology_link;
	}

	@Override
	public String toString() {
		return "OpenHab_Ontology_Link [openhab_item=" + openhab_item + ", Ontology_object=" + Ontology_object.toString_Normal()
				+ ", openhab_Ontology_link=" + openhab_Ontology_link + "]";
	}

	@Override
	public boolean equals(Object obj) {
		OpenHab_Ontology_Link link = (OpenHab_Ontology_Link) obj;
		System.out.println(link +"  "+getOpenhab_Ontology_link());
		return link.getOpenhab_Ontology_link().equals(getOpenhab_Ontology_link());
	}
	
	
}
