package com.DHBW.Mapper;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.DHBW.Ontology.Ontology_Bridge;
import com.DHBW.Ontology.Ontology_Object;
import com.DHBW.OpenHab.OpenHab_Bridge;
import com.DHBW.OpenHab.OpenHab_Item;
import com.DHBW.OpenHab.OpenHab_MQTT;

public class OpenHab_Ontology_Mapper extends OpenHab_MQTT implements Observer {

	private OpenHab_Bridge openhab_bridge;
	private Ontology_Bridge Ontology_bridge;
	private Hashtable<String, OpenHab_Ontology_Link> openhab_Ontology_links;

	public OpenHab_Ontology_Mapper(String ontology_file_path) throws Exception {
		super();
		this.openhab_bridge = new OpenHab_Bridge();
		this.Ontology_bridge = new Ontology_Bridge(ontology_file_path);
		this.openhab_Ontology_links = new Hashtable<String, OpenHab_Ontology_Link>();
		initialization();
	}

	public void initialization() throws Exception {
		AddObserver_Ontology();
		Ontology_Map_OpenHab();
		Initialization_Helper();
	}

	public void AddObserver_Ontology() {
		for (Ontology_Object Ontology_object : Ontology_bridge.Parse_Ontology_Object()) {
			Ontology_object.addObserver(this);
		}
	}

	public void Ontology_Map_OpenHab() throws Exception {
		ArrayList<Ontology_Object> Ontology_Objects = Ontology_bridge.Parse_Ontology_Object();
		ArrayList<OpenHab_Item> OpenHab_Items = openhab_bridge.getItems();
		for (OpenHab_Item openHab_Item : OpenHab_Items) {
			String openhab_item_link = openHab_Item.getTagsconcatenated();
			for (Ontology_Object Ontology_Object : Ontology_Objects) {
				String Ontology_object_link = Ontology_Object.getOpenhab_identifier();
				if (Ontology_object_link.equals(openhab_item_link)) {
					OpenHab_Ontology_Link openhab_Ontology_link = new OpenHab_Ontology_Link(openHab_Item, Ontology_Object,
							Ontology_object_link);
					openhab_Ontology_links.put(Ontology_object_link, openhab_Ontology_link);
					break;
				}
			}
		}
	}

	public void Initialization_Helper() throws Exception {
		unsubscribe();
		Set<String> openhab_Ontology_links_keyset = openhab_Ontology_links.keySet();
		for (String openhab_Ontology_links_key : openhab_Ontology_links_keyset) {
			OpenHab_Ontology_Link openHab_Ontology_Link = openhab_Ontology_links.get(openhab_Ontology_links_key);
			Ontology_Object Ontology_object = openHab_Ontology_Link.getOntology_object();
			OpenHab_Item openhab_item = openHab_Ontology_Link.getOpenhab_item();
			String Ontology_value = Ontology_object.getOntology_data_literal();
			System.out.println(openhab_item.getName()+" "+Ontology_value);
			openhab_bridge.setState(openhab_item.getName(), Ontology_value);
		}
		subscribe();
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		String openhab_item_name = topic;
		OpenHab_Item openhab_item = openhab_bridge.getItem(openhab_item_name);
		OpenHab_Ontology_Link openhab_Ontology_link = openhab_Ontology_links.get(openhab_item.getTagsconcatenated());
		Ontology_Object Ontology_object = openhab_Ontology_link.getOntology_object();
		System.out.println(topic+" "+message);
		Ontology_bridge.Edit_Ontology_Object(Ontology_object, message.toString());
		// Ontology_bridge.Display_Ontology_Objects();
	}

	@Override
	public void update(Observable o, Object arg) {
		try {
			unsubscribe();
			Ontology_Object Ontology_object = (Ontology_Object) arg;
			String OpenHab_Identifier=Ontology_object.getOpenhab_identifier();
			OpenHab_Ontology_Link openhab_Ontology_link = openhab_Ontology_links.get(OpenHab_Identifier);
			OpenHab_Item openhab_item = openhab_Ontology_link.getOpenhab_item();
			String Ontology_value = Ontology_object.getOntology_data_literal();
			openhab_bridge.setState(openhab_item.getName(), Ontology_value);
			subscribe();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}