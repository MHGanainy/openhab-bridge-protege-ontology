# Project Setup
1. Install OpenHAB from https://www.openhab.org/download/ choose windows and version 2.4.0
2. Install Latest Java 8 platform
3. Follow the instruction of setting up openHAB on Windows https://www.openhab.org/docs/installation/windows.html
4. Run the Start.bat in OpenHAB setup folder and then open http://localhost:8080
5. For first Time choose standard setup
6. Go to Paper UI-> Add-ons -> MISC -> Type in the search bar MQTT and install Embedded MQTT broker
7. Go to Paper UI-> Add-ons -> Bindings -> Type in the search bar MQTT and install MQTT Binding and go to transformation and install JSONPATH Transformation
8. Go to inbox and add the new binding as a thing
9. Go to Things under the configuration tab and check if the MQTT Broker is Online if not make sure that Port 1883 is free to listen on
10. Install Latest Java JDK for Eclipse
11. Install Latest Eclipse Version
12. Install Gitbash from https://git-scm.com/download/win
13. Create New Folder for the Project and right click inside it Git Bash Here
14. type the Command "git clone https://gitlab.com/MHGanainy/openhab-bridge-protege-ontology.git"
15. Install Maven for windows and check that the M2_HOME Variable is populated with the right the location and that the Path variable contains the Right path to the maven bin directory this can be done by typing the command "mvn --version" on CMD
16. Open Eclipse and Import project as an exsisting Maven Project
17. Go to the Pom file and right click on it and go to run as and press "Maven Clean" it should display build successful
18. Navigate to the cloned github repositry and open OpenHab-Configuration and copy the Folder "conf"
19. Navigate to the OpenHAB setup folder and replace the conf file
20. You need to restart the openHAB again after the preious step
21. Go to the src/main/java -> Main -> Main.java
22. run the java file and redirect to the basic UI in localhost:8080
